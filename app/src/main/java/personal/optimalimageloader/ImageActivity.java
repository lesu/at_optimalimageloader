package personal.optimalimageloader;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageActivity extends AppCompatActivity {
    private ImageView imageView;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        this.initComponents();
    }

    private void initComponents() {
        this.imageView = (ImageView) this.findViewById(R.id.image_view);
        this.textView = (TextView) this.findViewById(R.id.text_view);
        int image = R.drawable.image;
        ImageOptimizer.loadImageToImageView(this, this.imageView,image);



    }

}
